<?php

namespace Hijauasri\Search;

use Illuminate\Support\Collection;

class Search
{
    protected $string;

    public function __construct($string) {

        $this->string = $string;
    }

    public function results(): Collection
    {
        return [];
    }
}